/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Control;

import ca.qc.colval.javase.projet2.Model.CountryListItem;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author 1401959
 */
public class CountryAdapter {
    
    ICountryDAO dao;

    public CountryAdapter(ICountryDAO dao) {
        this.dao = dao;
    }
    
    public ObservableList<CountryListItem> getEmptyList() {
        return FXCollections.emptyObservableList();
    }
    
    public ObservableList<CountryListItem> getCountryAndCodes() {
        List<CountryListItem> countryCodes = dao.fetchAllCountriesAndCodes();
        ObservableList<CountryListItem> oCountryCodes = FXCollections
                .observableList(countryCodes);
        return oCountryCodes;
    }
}
