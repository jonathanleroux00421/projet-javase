/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Service;

import ca.qc.colval.javase.projet2.View.GenerateBarcodeScene;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author Jonathan Leroux
 */
public class BarcodeDrawingService {
    private static GraphicsContext gc;
    
    public static void drawBarcode(String[] barcode) {
        double x = 45;
        double y = 25;
        final double w = 3;
        final double h = 100;
        
        gc = GenerateBarcodeScene.getInstance().canvas.getGraphicsContext2D();
        // Resetting the canvas to a white rectangle.
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, 375, 150);
        
        for (char c : barcode[0].toCharArray()) {
            if (c == '1')
                gc.setFill(Color.BLACK);
            else
                gc.setFill(Color.WHITE);
            gc.fillRect(x, y, w, h);
            x += 3;
        }
        
        x = 117.5;
        y = 140;
        final double max = 10;
        
        for (char c : barcode[1].toCharArray()) {
            switch (c) {
                case '0':
                    gc.fillText("0", x, y, max);
                    break;
                case '1':
                    gc.fillText("1", x, y, max);
                    break;
                case '2':
                    gc.fillText("2", x, y, max);
                    break;
                case '3':
                    gc.fillText("3", x, y, max);
                    break;
                case '4':
                    gc.fillText("4", x, y, max);
                    break;
                case '5':
                    gc.fillText("5", x, y, max);
                    break;
                case '6':
                    gc.fillText("6", x, y, max);
                    break;
                case '7':
                    gc.fillText("7", x, y, max);
                    break;
                case '8':
                    gc.fillText("8", x, y, max);
                    break;
                case '9':
                    gc.fillText("9", x, y, max);
                    break;
                case '-':
                    gc.fillText("-", x, y, max);
                    break;
            }
            
            x += 10;
        }
    }
}
