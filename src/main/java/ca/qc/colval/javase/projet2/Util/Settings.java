/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Jonathan Leroux
 */
public class Settings {
    private static Properties settings = null;

    public static Properties getSettings() {
        if (settings == null) {
            FileInputStream in;
            try {
                settings = new Properties();
                File file = new File("src/main/resources/XML/Settings.xml");
                in = new FileInputStream(file);
                settings.loadFromXML(in);
                in.close();
            } catch (IOException ex) {
                Logger.post(ex.getMessage());
            }
        }
        return settings;
    }
}
