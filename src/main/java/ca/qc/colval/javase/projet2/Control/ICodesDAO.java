/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Control;

/**
 *
 * @author Jonathan Leroux <jonathan.leroux at edu.colval.qc.ca>
 */
public interface ICodesDAO {
    public String getEncodingPattern(int digit);
    public String getLCode(int digit);
    public String getGCode(int digit);
    public String getRCode(int digit);
}
