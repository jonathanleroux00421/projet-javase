/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Control;

import ca.qc.colval.javase.projet2.Data.DataService;
import ca.qc.colval.javase.projet2.Model.EncodingHistoryItem;

/**
 *
 * @author 1401959
 */
public class BarcodeHistoryConverter {
    public static void convertItemAndAddEntry(String decimal, String binary) {
        DataService
                .getInstance()
                .getHistoryDAO()
                .addEntry(new EncodingHistoryItem(DataService.getInstance()
                        .getHistoryDAO().findNextId(), decimal, binary));
    }
}
