/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Model;

import org.bson.types.ObjectId;

/**
 *
 * @author Jonathan Leroux
 */
public class EncodingHistoryItem {
    private ObjectId id;
    private int customId;
    private String decimalBarcode;
    private String binaryBarcode;
    
    public EncodingHistoryItem() {
        
    }

    public EncodingHistoryItem(int customId, String decimalBarcode, String binaryBarcode) {
        this.customId = customId;
        this.decimalBarcode = decimalBarcode;
        this.binaryBarcode = binaryBarcode;
    }

    public String getDecimalBarcode() {
        return decimalBarcode;
    }

    public void setDecimalBarcode(String decimalBarcode) {
        this.decimalBarcode = decimalBarcode;
    }

    public String getBinaryBarcode() {
        return binaryBarcode;
    }

    public void setBinaryBarcode(String binaryBarcode) {
        this.binaryBarcode = binaryBarcode;
    }

    public int getCustomId() {
        return customId;
    }

    public void setCustomId(int customId) {
        this.customId = customId;
    }
    
    @Override
    public String toString() {
        return "EncodingHistoryItem{" + "decimalBarcode=" + decimalBarcode + ", binaryBarcode=" + binaryBarcode + '}';
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }
}
