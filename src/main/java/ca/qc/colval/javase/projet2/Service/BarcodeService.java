/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Service;

import ca.qc.colval.javase.projet2.Model.BarcodeEngine;
import ca.qc.colval.javase.projet2.Util.Logger;
import ca.qc.colval.javase.projet2.Util.Settings;
import ca.qc.colval.javase.projet2.View.GenerateBarcodeScene;
import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;
import static net.andreinc.mockneat.unit.types.Ints.ints;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author 1401959
 */
public class BarcodeService {

    private static BarcodeEngine mock = null;

    public static String encode() {
        if (Boolean.getBoolean(Settings
                .getSettings()
                .getProperty("STUBBING")) == true)
            return getInstance().getBinary();
        else
            return BinaryCodeService
                    .generateBinaryEncoding(
            GenerateBarcodeScene
                    .getInstance()
                    .countryCodeBox
                    .getValue()
                    .toString()
                    + GenerateBarcodeScene
                            .getInstance()
                            .manufacturerCodeField
                            .getText()
                    + GenerateBarcodeScene
                            .getInstance()
                            .productCodeField
                            .getText());
    }

    public static String decode() {
        if (Boolean.getBoolean(Settings
                .getSettings()
                .getProperty("STUBBING")) == true)
            return getInstance().getDecimal();
        else
            return GenerateBarcodeScene
                    .getInstance()
                    .countryCodeBox
                    .getValue()
                    .toString()
                    + "-"
                    + GenerateBarcodeScene
                            .getInstance()
                            .manufacturerCodeField
                            .getText()
                    + "-"
                    + GenerateBarcodeScene
                            .getInstance()
                            .productCodeField
                            .getText();
    }

    public static String[] getBarcode() {
        String[] result = {encode(), decode()};
        return result;
    }

    public static void getPDF(Canvas canvas) {
        WritableImage contentCopy = canvas.snapshot(new SnapshotParameters(), null);
        Canvas copy = new Canvas();
        copy.getGraphicsContext2D().drawImage(contentCopy, 0, 0);
        Pane pane = new Pane(copy);
        WritableImage image = pane.snapshot(new SnapshotParameters(), null);
        File file = new File("src/main/resources/PDF/image.png");
        
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException e) {
            Logger.post(e.getMessage());
        }
        
        PDDocument doc = new PDDocument();
        PDPage page = new PDPage();
        PDImageXObject pdimage;
        PDPageContentStream content;
        
        try {
            pdimage = PDImageXObject.createFromFile("src/main/resources/PDF/"
                    + "image.png", doc);
            content = new PDPageContentStream(doc, page);
            content.drawImage(pdimage, 25, 620);
            content.close();
            doc.addPage(page);
            doc.save("src/main/resources/PDF/Barcode.pdf");
            doc.close();
            file.delete();
        } catch (IOException e) {
            Logger.post(e.getMessage());
        }
    }

    ///<editor-fold defaultstate="collapsed" desc="ONLY USED FOR STUBBING">
    // This method is only used to generate random barcode decimal and binary values.
    //</editor-fold>
    private static BarcodeEngine getInstance() {
        if (mock == null) {
            mock = mock(BarcodeEngine.class);
            String decimal = "";
            StringBuilder binary;
            
            for (int i = 0; i < 14; i++) {
                if (i == 1 | i == 7)
                    decimal += "-";
                else
                    decimal += String.valueOf((int)ints().bound(10).get());
            }
            when(mock.getDecimal()).thenReturn(decimal);
            
            binary = new StringBuilder("101");
            ints()
                    .bound(2)
                    .list(42)
                    .get()
                    .forEach((i) -> {
                binary.append(String.valueOf((int)i));
            });
            binary.append("01010");
            ints()
                    .bound(2)
                    .list(42)
                    .get()
                    .forEach((i) -> {
                binary.append(String.valueOf((int)i));
            });
            binary.append("101");
            
            when(mock.getBinary()).thenReturn(binary.toString());
        }
        return mock;
    }
}
