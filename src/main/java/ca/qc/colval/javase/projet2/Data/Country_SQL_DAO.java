/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Data;

import ca.qc.colval.javase.projet2.Control.ICountryDAO;
import ca.qc.colval.javase.projet2.Model.CountryListItem;
import ca.qc.colval.javase.projet2.Util.Logger;
import ca.qc.colval.javase.projet2.Util.ApplicationProperties;
// Java MySQL JDBC
import com.mysql.cj.jdbc.MysqlDataSource;
// Java SQL
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
// Java Utilitaries
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
// MockNeat
import static net.andreinc.mockneat.unit.address.Countries.countries;
import static net.andreinc.mockneat.unit.types.Ints.ints;

/**
 *
 * @author Jonathan Leroux <jonathan.leroux at edu.colval.qc.ca>
 */
public class Country_SQL_DAO implements ICountryDAO {

    private static Connection connection = null;
    private static Statement statement = null;

    @Override
    public void populate() {
        createConnection();

        //<editor-fold defaultstate="collapsed" desc="MockNeat data generation">
        // MockNeat random data generation to populate database.
        //</editor-fold>
        Supplier<List<Integer>> makeValues = () -> ints()
                .bound(999)
                .list(ints().range(1, 5))
                .get();
        Map<String, List<Integer>> test = countries()
                .names()
                .mapVals(2000, makeValues)
                .get();
        Map<String, List<Integer>> sorted = test
                .entrySet()
                .stream()
                .sorted(Map.Entry
                        .comparingByKey())
                .collect(Collectors
                        .toMap(e -> e.getKey(),
                                e -> e.getValue(),
                                (e1, e2) -> e1,
                                LinkedHashMap::new));

        // //<editor-fold defaultstate="collapsed" desc="SQL Insert statement">
        // SQL Insert statements section.
        //</editor-fold>
        int countryId = 0;
        int codeId = 0;

        try {
            statement = connection.createStatement();

            for (Map.Entry<String, List<Integer>> entries : sorted.entrySet()) {
                countryId++;
                String query = "INSERT INTO "
                        + ApplicationProperties
                                .getAppProperties()
                                .getProperty("COUNTRIES_TABLE") + " ("
                        + ApplicationProperties
                                .getAppProperties()
                                .getProperty("ID_COLUMN") + ", "
                        + ApplicationProperties
                                .getAppProperties()
                                .getProperty("NAME_COLUMN") + ") VALUES("
                        + countryId + ",'"
                        + entries
                                .getKey()
                                .replace("'", "''")
                        + "')";
                statement.addBatch(query);
                for (Integer code : entries.getValue()) {
                    codeId++;
                    query = "INSERT INTO "
                            + ApplicationProperties
                                    .getAppProperties()
                                    .getProperty("CODES_TABLE") + " ("
                            + ApplicationProperties
                                    .getAppProperties()
                                    .getProperty("ID_COLUMN") + ", "
                            + ApplicationProperties
                                    .getAppProperties()
                                    .getProperty("CODE_COLUMN") + ") VALUES("
                            + codeId + "," + code + ")";
                    statement.addBatch(query);
                    query = "INSERT INTO "
                            + ApplicationProperties
                                    .getAppProperties()
                                    .getProperty("COUNTRIES_CODES_TABLE")
                            + " ("
                            + ApplicationProperties
                                    .getAppProperties()
                                    .getProperty("COUNTRYID_COLUMN") + ", "
                            + ApplicationProperties
                                    .getAppProperties()
                                    .getProperty("CODEID_COLUMN") + ") VALUES("
                            + countryId + "," + codeId + ")";
                    statement.addBatch(query);
                }
            }

            statement.executeBatch();

        } catch (SQLException e) {
            Logger.post(e.getMessage() + "\n" + e.getNextException());
        } finally {
            closeConnection();
        }
    }

    @Override
    public void createConnection() {
        try {
            MysqlDataSource dataSource = new MysqlDataSource();
            dataSource.setUser(ApplicationProperties
                    .getAppProperties().getProperty("COUNTRY_SQL_USERNAME"));
            dataSource.setPassword(ApplicationProperties
                    .getAppProperties().getProperty("COUNTRY_SQL_PASSWORD"));
            dataSource.setServerName(ApplicationProperties
                    .getAppProperties().getProperty("COUNTRY_SQL_SERVER_NAME"));
            dataSource.setPortNumber(Integer.valueOf(ApplicationProperties
                    .getAppProperties().getProperty("COUNTRY_SQL_PORT_NUMBER")));
            dataSource.setDatabaseName(ApplicationProperties
                    .getAppProperties().getProperty("COUNTRY_SQL_DATABASE_NAME"));
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            Logger.post(e.getMessage());
            closeConnection();
        }
    }

    @Override
    public List<String> fetchAllCountries() {
        List<String> countries = new LinkedList();
        createConnection();

        try {
            statement = connection.createStatement();
            String query = "SELECT " + ApplicationProperties
                    .getAppProperties().getProperty("NAME_COLUMN") + " FROM "
                    + ApplicationProperties.getAppProperties()
                            .getProperty("COUNTRIES_TABLE");
            statement.execute(query);
            ResultSet rs = statement.getResultSet();

            while (rs.next()) {
                countries.add(rs.getString(ApplicationProperties
                        .getAppProperties().getProperty("NAME_COLUMN")));
            }

        } catch (SQLException e) {
            Logger.post(e.getMessage());
        } finally {
            closeConnection();
        }

        return countries;
    }

    @Override
    public List<Integer> fetchAllCodesByCountryName(String country) {
        List<Integer> countryCodes = new LinkedList();
        createConnection();

        // Cela aurait été mieux d'avoir le query dans les propriétés.
        try {
            statement = connection.createStatement();
            String query = "SELECT " + ApplicationProperties
                    .getAppProperties().getProperty("CODE_COLUMN") + " "
                    + "FROM " + ApplicationProperties
                            .getAppProperties()
                            .getProperty("CODES_TABLE") + " "
                    + "INNER JOIN " + ApplicationProperties
                            .getAppProperties()
                            .getProperty("COUNTRIES_CODES_TABLE") + " "
                    + "ON " + ApplicationProperties
                            .getAppProperties()
                            .getProperty("CODEID_COLUMN") + "="
                    + ApplicationProperties
                            .getAppProperties()
                            .getProperty("CODES_TABLE_ID_COLUMN") + " "
                    + "INNER JOIN " + ApplicationProperties
                            .getAppProperties()
                            .getProperty("COUNTRIES_TABLE") + " "
                    + "ON " + ApplicationProperties
                            .getAppProperties()
                            .getProperty("COUNTRYID_COLUMN") + "="
                    + ApplicationProperties
                            .getAppProperties()
                            .getProperty("COUNTRIES_TABLE_ID_COLUMN") + " "
                    + "WHERE " + ApplicationProperties
                            .getAppProperties()
                            .getProperty("NAME_COLUMN") + "=\'"
                    + country + "\'";
            statement.execute(query);
            ResultSet rs = statement.getResultSet();

            while (rs.next()) {
                countryCodes.add(rs.getInt(ApplicationProperties
                        .getAppProperties().getProperty("CODE_COLUMN")));
            }

        } catch (SQLException e) {
            Logger.post(e.getMessage());
        } finally {
            closeConnection();
        }

        return countryCodes;
    }

    @Override
    public List<CountryListItem> fetchAllCountriesAndCodes() {
        List<CountryListItem> countriesCodes = new LinkedList();
        createConnection();

        try {
            statement = connection.createStatement();
            String query = "SELECT " + ApplicationProperties.getAppProperties()
                    .getProperty("NAME_COLUMN") + ", "
                    + ApplicationProperties.getAppProperties()
                            .getProperty("CODE_COLUMN") + " FROM "
                    + ApplicationProperties.getAppProperties()
                            .getProperty("COUNTRIES_CODES_TABLE") + " INNER JOIN "
                    + ApplicationProperties.getAppProperties()
                            .getProperty("COUNTRIES_TABLE") + " ON "
                    + ApplicationProperties.getAppProperties()
                            .getProperty("COUNTRIES_TABLE_ID_COLUMN") + "="
                    + ApplicationProperties.getAppProperties()
                            .getProperty("COUNTRYID_COLUMN") + " INNER JOIN "
                    + ApplicationProperties.getAppProperties()
                            .getProperty("CODES_TABLE") + " ON "
                    + ApplicationProperties.getAppProperties()
                            .getProperty("CODES_TABLE_ID_COLUMN") + "="
                    + ApplicationProperties.getAppProperties()
                            .getProperty("CODEID_COLUMN");
            statement.execute(query);
            ResultSet rs = statement.getResultSet();

            while (rs.next()) {
                CountryListItem newItem = new CountryListItem(
                        rs.getString(ApplicationProperties.getAppProperties()
                                .getProperty("NAME_COLUMN")),
                        rs.getInt(ApplicationProperties.getAppProperties()
                                .getProperty("CODE_COLUMN")));
                countriesCodes.add(newItem);
            }

        } catch (SQLException e) {
            Logger.post(e.getMessage());
        } finally {
            closeConnection();
        }
        return countriesCodes;
    }

    @Override
    public void closeConnection() {
        try {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            Logger.post(e.getMessage());
        }
    }
}
