/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.View;

import ca.qc.colval.javase.projet2.Control.CountryAdapter;
import ca.qc.colval.javase.projet2.Data.DataService;
import ca.qc.colval.javase.projet2.Model.CountryListItem;
import ca.qc.colval.javase.projet2.Presenter.FindCountryCodePresenter;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author 1401959
 */
public class FindCountryCodesScene {
    private static FindCountryCodesScene instance = null;
    
    public Scene scene;
    HBox root;
    VBox buttonsBox;
    TableView<CountryListItem> table;
    public Button backButton;
    public Button exitButton;
    CountryAdapter dataAdapter;

    public FindCountryCodesScene() {
        // Setting up HBox as root element.
        root = new HBox();
        root.setPadding(new Insets(25));
        root.setSpacing(20);
        
        // Setting up a table view.
        table = new TableView();
        
        TableColumn<CountryListItem, String> countryColumn = new TableColumn("Country");
        countryColumn.setMinWidth(150);
        countryColumn.setCellValueFactory(new PropertyValueFactory("name"));
        
        TableColumn<CountryListItem, Integer> ean13CodeColumn = new TableColumn("EAN13 Code");
        ean13CodeColumn.setMinWidth(50);
        ean13CodeColumn.setCellValueFactory(new PropertyValueFactory("code"));
        
        table.getColumns().addAll(countryColumn, ean13CodeColumn);
        
        dataAdapter = new CountryAdapter(DataService.getInstance().getCountryDAO());
        table.setItems(dataAdapter.getCountryAndCodes());
        
        // Setting up buttons.
        backButton = new Button("Back");
        backButton.setOnAction(FindCountryCodePresenter.getInstance());
        backButton.setMaxWidth(100);
        exitButton = new Button("Exit");
        exitButton.setOnAction(FindCountryCodePresenter.getInstance());
        exitButton.setMinWidth(100);
        buttonsBox = new VBox();
        buttonsBox.setSpacing(20);
        buttonsBox.getChildren().addAll(backButton, exitButton);
        
        root.getChildren().addAll(table, buttonsBox);
        
        scene = new Scene(root, 450, 400);
    }
    
    public static FindCountryCodesScene getInstance() {
        if (instance == null)
            instance = new FindCountryCodesScene();
        return instance;
    }
}
