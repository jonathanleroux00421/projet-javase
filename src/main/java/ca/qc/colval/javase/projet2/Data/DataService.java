/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Data;

import ca.qc.colval.javase.projet2.Control.ICodesDAO;
import ca.qc.colval.javase.projet2.Control.ICountryDAO;
import ca.qc.colval.javase.projet2.Control.IHistoryDAO;

/**
 *
 * @author Jonathan Leroux <jonathan.leroux at edu.colval.qc.ca>
 */
public class DataService {
    
    private static DataService instance = null;
    private final ICodesDAO CodesDAO;
    private final IHistoryDAO HistoryDAO;
    private final ICountryDAO CountryDAO;
    
    public static DataService getInstance() {
        if (instance == null)
            instance = new DataService();
        return instance;
    }
    
    protected DataService() {
        CodesDAO = new Codes_XML_DAO();
        HistoryDAO = new History_JSON_DAO();
        CountryDAO = new Country_SQL_DAO();
    }
    
    public ICodesDAO getCodesDAO() {
        return CodesDAO;
    }
    
    public IHistoryDAO getHistoryDAO() {
        return HistoryDAO;
    }
    
    public ICountryDAO getCountryDAO() {
        return CountryDAO;
    }
}
