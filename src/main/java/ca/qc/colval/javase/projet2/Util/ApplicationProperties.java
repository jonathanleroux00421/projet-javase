/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Jonathan Leroux
 */
public class ApplicationProperties {

    private static Properties appProperties = null;

    public static Properties getAppProperties() {
        if (appProperties == null) {
            FileInputStream in;
            try {
                appProperties = new Properties();
                File file = new File("src/main/resources/XML/Properties.xml");
                in = new FileInputStream(file);
                appProperties.loadFromXML(in);
                in.close();
            } catch (IOException ex) {
                Logger.post(ex.getMessage());
            }
        }
        return appProperties;
    }
}
