/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Data;

// Project packages.
import ca.qc.colval.javase.projet2.Control.IHistoryDAO;
import ca.qc.colval.javase.projet2.Model.EncodingHistoryItem;
import ca.qc.colval.javase.projet2.Util.ApplicationProperties;
// Google packages.
import com.google.gson.JsonArray;
import com.mongodb.BasicDBObject;
// MongoDB packages.
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import static com.mongodb.client.model.Filters.eq;
// Java Utils.
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
// BSON packages.
import org.bson.codecs.configuration.CodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import org.bson.codecs.pojo.PojoCodecProvider;

/**
 *
 * @author Jonathan Leroux <jonathan.leroux at edu.colval.qc.ca>
 */
public class History_JSON_DAO implements IHistoryDAO {
    
    CodecRegistry codecRegistry;
    MongoClient mongoClient;
    MongoDatabase db;
    MongoClientSettings settings;
    Properties properties;
    
    public History_JSON_DAO() {
        properties = ApplicationProperties.getAppProperties();
        codecRegistry = fromRegistries(MongoClientSettings
                .getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider
                        .builder()
                        .automatic(true)
                        .build()));
        
        MongoCredential credential = MongoCredential.createCredential(
                properties.getProperty("JSON_USERNAME"),
                properties.getProperty("JSON_DATABASE"),
                properties.getProperty("JSON_PASSWORD").toCharArray());
        
        List<ServerAddress> hosts = new ArrayList<>();
        hosts.add(new ServerAddress(properties.getProperty("JSON_HOST"),
                Integer.valueOf(properties.getProperty("JSON_PORT_NUMBER"))));
        
        settings = MongoClientSettings.builder()
                .credential(credential)
                .applyToClusterSettings(builder -> builder.hosts(hosts))
                .build();
    }
    
    private void createConnection() {
        mongoClient = MongoClients.create(settings);
        db = mongoClient.getDatabase(properties.getProperty("JSON_DATABASE"))
                .withCodecRegistry(codecRegistry);
    }
    
    private void closeConnection() {
        mongoClient.close();
    }
    
    @Override
    public void addEntry(EncodingHistoryItem newItem) {
        createConnection();
        MongoCollection<EncodingHistoryItem> collection = db
                .getCollection(properties.getProperty("JSON_COLLECTION"),
                        EncodingHistoryItem.class);
        collection.insertOne(newItem);
        closeConnection();
    }
    
    @Override
    public int findNextId() {
        createConnection();
        MongoCollection<EncodingHistoryItem> collection = db
                .getCollection(properties.getProperty("JSON_COLLECTION"),
                        EncodingHistoryItem.class);
        List<EncodingHistoryItem> list = collection.find()
                .into(new ArrayList<>());
        int result = 0;
        
        for (EncodingHistoryItem ehi : list) {
            if (ehi.getCustomId() > result)
                result = ehi.getCustomId();
        }
        
        result++;
        closeConnection();
        return result;
    }
    
    @Override
    public EncodingHistoryItem fetchOneBarcode(int id) {
        createConnection();
        MongoCollection<EncodingHistoryItem> collection = db
                .getCollection(properties.getProperty("JSON_COLLECTION"),
                        EncodingHistoryItem.class);
        EncodingHistoryItem result = collection.find(eq("customId", 1)).first();
        closeConnection();
        return result;
    }
}
