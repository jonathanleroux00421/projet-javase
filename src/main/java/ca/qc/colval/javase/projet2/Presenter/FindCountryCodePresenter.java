/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Presenter;

import ca.qc.colval.javase.projet2.View.FindCountryCodesScene;
import ca.qc.colval.javase.projet2.View.GenerateBarcodeScene;
import ca.qc.colval.javase.projet2.View.MainApp;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 *
 * @author 1401959
 */
public class FindCountryCodePresenter implements EventHandler<ActionEvent> {
    private static FindCountryCodePresenter instance = null;

    protected FindCountryCodePresenter() {
    }
    
    public static FindCountryCodePresenter getInstance() {
        if (instance == null)
            instance = new FindCountryCodePresenter();
        return instance;
    }
    
    @Override
    public void handle(ActionEvent event) {
        
        if (event.getSource() == FindCountryCodesScene.getInstance().backButton)
            MainApp.appStage.setScene(GenerateBarcodeScene.getInstance().scene);
        
        if (event.getSource() == FindCountryCodesScene.getInstance().exitButton)
            MainApp.appStage.close();
    }
}
