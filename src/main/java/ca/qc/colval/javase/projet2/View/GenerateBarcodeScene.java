/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.View;

// Project Packages
import ca.qc.colval.javase.projet2.Presenter.GenerateBarcodePresenter;
import ca.qc.colval.javase.projet2.Data.DataService;
import ca.qc.colval.javase.projet2.Util.Settings;
// Java FX packages
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author 1401959
 */
public class GenerateBarcodeScene {

    private static GenerateBarcodeScene instance = null;

    public Scene scene;
    VBox root;
    GridPane grid;
    Text countryLabel;
    Text countryCodeLabel;
    Text manufacturerCodeLabel;
    Text productCodeLabel;
    public ComboBox countryBox;
    public ComboBox countryCodeBox;
    public TextField manufacturerCodeField;
    public TextField productCodeField;
    public Button getBarcodeBtn;
    public Button getCountryCodesBtn;
    public Button getPDFBtn;
    public Button exitBtn;
    public Canvas canvas;
    GenerateBarcodePresenter eventHandler;

    protected GenerateBarcodeScene() {
        if (Boolean.getBoolean(Settings
                .getSettings()
                .getProperty("POPULATE")) == true) {
            DataService.getInstance().getCountryDAO().populate();
        }
        // Setting up a VBox as root element.
        root = new VBox();
        root.setAlignment(Pos.TOP_CENTER);

        // Setting up the grid.
        grid = new GridPane();
        grid.setVgap(20);
        grid.setHgap(30);
        grid.setPadding(new Insets(25, 25, 25, 25));

        // Setting up the labels.
        countryLabel = new Text("Country :");
        countryLabel.setFont(Font.font("Tahoma", FontWeight.NORMAL, 14));
        countryCodeLabel = new Text("Country code :");
        countryCodeLabel.setFont(Font.font("Tahoma", FontWeight.NORMAL, 14));
        manufacturerCodeLabel = new Text("Manufacturer code :");
        manufacturerCodeLabel.setFont(Font.font("Tahoma", FontWeight.NORMAL, 14));
        productCodeLabel = new Text("Product code :");
        productCodeLabel.setFont(Font.font("Tahoma", FontWeight.NORMAL, 14));

        // Setting up the combo box and text fields.
        countryBox = new ComboBox();
        countryBox.getItems().addAll(DataService
                .getInstance()
                .getCountryDAO()
                .fetchAllCountries());
        countryBox.setMinWidth(150);
        countryCodeBox = new ComboBox();
        countryCodeBox.setMinWidth(175);
        countryBox.getSelectionModel().selectedIndexProperty().addListener(
                new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object old, Object newValue) {
                countryCodeBox
                        .setItems(FXCollections.observableList(
                        DataService
                                .getInstance()
                                .getCountryDAO()
                                .fetchAllCodesByCountryName(countryBox
                                        .getValue()
                                        .toString())));
                countryCodeBox
                        .getSelectionModel()
                        .selectFirst();
            }});
        countryBox.getSelectionModel().selectFirst();
        manufacturerCodeField = new TextField();
        manufacturerCodeField.setMinWidth(125);
        productCodeField = new TextField();
        productCodeField.setMinWidth(125);

        // Setting up a custom EventHandler for the button actions.
        eventHandler = GenerateBarcodePresenter.getInstance();
        // Setting up the buttons.
        getBarcodeBtn = new Button("Get Barcode");
        getBarcodeBtn.setMinWidth(125);
        getBarcodeBtn.setOnAction(eventHandler);
        getCountryCodesBtn = new Button("Country Codes");
        getCountryCodesBtn.setMinWidth(125);
        getCountryCodesBtn.setOnAction(eventHandler);
        getPDFBtn = new Button("Download to PDF");
        getPDFBtn.setMinWidth(125);
        getPDFBtn.setOnAction(eventHandler);
        exitBtn = new Button("Exit");
        exitBtn.setMinWidth(125);
        exitBtn.setOnAction(eventHandler);

        // Setting up a canvas to draw a barcode.
        canvas = new Canvas(375, 150);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, 375, 150);

        // Adding labels to grid.
        grid.add(countryLabel, 0, 0);
        grid.add(countryCodeLabel, 0, 1);
        grid.add(manufacturerCodeLabel, 0, 2);
        grid.add(productCodeLabel, 0, 3);
        // Adding comboBox and textFields to grid.
        grid.add(countryBox, 1, 0);
        grid.add(countryCodeBox, 1, 1);
        grid.add(manufacturerCodeField, 1, 2);
        grid.add(productCodeField, 1, 3);
        // Adding buttons to grid.
        grid.add(getBarcodeBtn, 2, 0);
        grid.add(getCountryCodesBtn, 2, 1);
        grid.add(getPDFBtn, 2, 2);
        grid.add(exitBtn, 2, 3);
        // Adding a separator.
        grid.add(new Separator(), 0, 4, 3, 1);
        // Adding grid to root.
        root.getChildren().add(grid);
        // Adding canvas to root.
        root.getChildren().add(canvas);

        scene = new Scene(root, 525, 425);
        scene.getStylesheets().add("styles/Styles.css");
    }

    public static GenerateBarcodeScene getInstance() {
        if (instance == null) {
            instance = new GenerateBarcodeScene();
        }
        return instance;
    }
}
