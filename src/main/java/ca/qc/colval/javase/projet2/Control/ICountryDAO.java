/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Control;

import ca.qc.colval.javase.projet2.Model.CountryListItem;
import java.util.List;

/**
 *
 * @author Jonathan Leroux
 */
public interface ICountryDAO {
    public void createConnection();
    public void closeConnection();
    public void populate();
    public List<String> fetchAllCountries();
    public List<Integer> fetchAllCodesByCountryName(String country);
    public List<CountryListItem> fetchAllCountriesAndCodes();
}
