/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Data;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import ca.qc.colval.javase.projet2.Control.ICodesDAO;

/**
 *
 * @author Jonathan Leroux <jonathan.leroux at edu.colval.qc.ca>
 */
public class Codes_XML_DAO implements ICodesDAO {

    DocumentBuilderFactory factory;
    DocumentBuilder builder;
    File xmlFile;
    Document xml;
    Element root;
    XPathFactory xpf;
    XPath path;

    public Codes_XML_DAO() {
        try {
            factory = DocumentBuilderFactory.newInstance();
            xmlFile = new File("src/main/resources/XML/EAN13.xml");
            builder = factory.newDocumentBuilder();
            xml = builder.parse(xmlFile);
            root = xml.getDocumentElement();
            xpf = XPathFactory.newInstance();
            path = xpf.newXPath();
        } catch (IOException | ParserConfigurationException | SAXException e) {
            System.out.println(e.getMessage());
        }
    }
    
    @Override
    public String getEncodingPattern(int digit) {
        String result = null;
        try {
            String expression = "//EncodingPattern[@CountryDigit=\"" + digit +
                    "\"]/@LeftGroupPattern";
            result = (String)path.evaluate(expression, root);
        } catch (XPathExpressionException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }

    @Override
    public String getLCode(int digit) {
        String result = null;
        try {
            String expression = "//DigitEncoding[@Digit=\"" + digit
                    + "\"]/@L_Code";
            result = (String)path.evaluate(expression, root);
        } catch (XPathExpressionException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }

    @Override
    public String getGCode(int digit) {
        String result = null;
        try {
            String expression = "//DigitEncoding[@Digit=\"" + digit
                    + "\"]/@G_Code";
            result = (String)path.evaluate(expression, root);
        } catch (XPathExpressionException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }

    @Override
    public String getRCode(int digit) {
        String result = null;
        try {
            String expression = "//DigitEncoding[@Digit=\"" + digit
                    + "\"]/@R_Code";
            result = (String)path.evaluate(expression, root);
        } catch (XPathExpressionException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }
}
