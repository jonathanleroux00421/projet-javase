/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Control;

import ca.qc.colval.javase.projet2.Model.EncodingHistoryItem;

/**
 *
 * @author Jonathan Leroux
 */
public interface IHistoryDAO {
    public void addEntry(EncodingHistoryItem newItem);
    public int findNextId();
    public EncodingHistoryItem fetchOneBarcode(int id);
}
