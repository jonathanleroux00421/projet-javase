/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Service;

import ca.qc.colval.javase.projet2.Data.DataService;

/**
 *
 * @author Jonathan Leroux
 */
public class BinaryCodeService {
    
    public static String generateBinaryEncoding(String decimalCode) {
        String pattern = getEncodingPattern(Character.getNumericValue(
                decimalCode.charAt(0)));
        String binary = "101";
        
        for (int i = 0; i < pattern.length(); i++) {
            switch (pattern.charAt(i)) {
                case 'L':
                    binary = binary + getLCode(Character.getNumericValue(decimalCode.charAt(i)));
                    break;
                case 'G':
                    binary = binary + getGCode(Character.getNumericValue(decimalCode.charAt(i)));
                    break;
                case 'R':
                    binary = binary + getRCode(Character.getNumericValue(decimalCode.charAt(i)));
                    break;
            }
            if (i == 6)
                binary = binary + "01010";
        }
        
        return binary + "101";
    }
    
    private static String getEncodingPattern(int digit) {
        return DataService
                .getInstance()
                .getCodesDAO()
                .getEncodingPattern(digit)
                + "RRRRRR";
    }
    
    private static String getLCode(int digit) {
        return DataService.getInstance().getCodesDAO().getLCode(digit);
    }
    
    private static String getGCode(int digit) {
        return DataService.getInstance().getCodesDAO().getGCode(digit);
    }
    
    private static String getRCode(int digit) {
        return DataService.getInstance().getCodesDAO().getRCode(digit);
    }
}
