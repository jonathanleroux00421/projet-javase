/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Presenter;

import ca.qc.colval.javase.projet2.Control.BarcodeHistoryConverter;
import ca.qc.colval.javase.projet2.Service.BarcodeService;
import ca.qc.colval.javase.projet2.Service.BarcodeDrawingService;
import ca.qc.colval.javase.projet2.View.FindCountryCodesScene;
import ca.qc.colval.javase.projet2.View.GenerateBarcodeScene;
import ca.qc.colval.javase.projet2.View.MainApp;
// Java FX Packages
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 *
 * @author 1401959
 */
public class GenerateBarcodePresenter implements EventHandler<ActionEvent> {
    private static GenerateBarcodePresenter instance = null;
    
    protected GenerateBarcodePresenter() {
    }
    
    public static GenerateBarcodePresenter getInstance() {
        if (instance == null)
            instance = new GenerateBarcodePresenter();
        return instance;
    }

    @Override
    public void handle(ActionEvent event) {
        
        if (event.getSource() == GenerateBarcodeScene.getInstance().exitBtn)
            MainApp.appStage.close();
        
        if (event.getSource() == GenerateBarcodeScene.getInstance().getBarcodeBtn) {
            BarcodeHistoryConverter.convertItemAndAddEntry(
                    BarcodeService.getBarcode()[1],
                    BarcodeService.getBarcode()[0]);
            BarcodeDrawingService.drawBarcode(BarcodeService.getBarcode());
        }
        
        if (event.getSource() == GenerateBarcodeScene.getInstance().getPDFBtn)
            BarcodeService.getPDF(GenerateBarcodeScene.getInstance().canvas);
        
        if (event.getSource() == GenerateBarcodeScene.getInstance().getCountryCodesBtn)
            MainApp.appStage.setScene(FindCountryCodesScene.getInstance().scene);
    }
}
