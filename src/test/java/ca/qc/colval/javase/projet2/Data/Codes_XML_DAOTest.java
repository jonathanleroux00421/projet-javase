/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Data;

// Project packages.
import ca.qc.colval.javase.projet2.Util.Logger;
// Junit5 packages.
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
// Mockito packages.
import org.mockito.Mockito;

/**
 *
 * @author Jonathan Leroux
 * Full Codes_XML_DAO class spy testing
 */
public class Codes_XML_DAOTest {
    
    static Codes_XML_DAO spy = null;

    @BeforeAll
    public static void initAll() {
        spy = Mockito.spy(Codes_XML_DAO.class);
    }
    
    /**
     * Test of getEncodingPattern method, of class Codes_XML_DAO.
     */
    @DisplayName("Encoding pattern")
    @Test
    public void testRealGetEncodingPattern() {
        // Arrange
        Logger.post("Testing getEncodingPattern method.");
        int digit0 = 0;
        int digit1 = 1;
        int digit2 = 2;
        int digit3 = 3;
        int digit4 = 4;
        int digit5 = 5;
        int digit6 = 6;
        int digit7 = 7;
        int digit8 = 8;
        int digit9 = 9;
        String expResult1 = "LLLLLL";
        String expResult2 = "LLGLGG";
        String expResult3 = "LLGGLG";
        String expResult4 = "LLGGGL";
        String expResult5 = "LGLLGG";
        String expResult6 = "LGGLLG";
        String expResult7 = "LGGGLL";
        String expResult8 = "LGLGLG";
        String expResult9 = "LGLGGL";
        String expResult10 = "LGGLGL";
        // Act
        String result1 = spy.getEncodingPattern(digit0);
        String result2 = spy.getEncodingPattern(digit1);
        String result3 = spy.getEncodingPattern(digit2);
        String result4 = spy.getEncodingPattern(digit3);
        String result5 = spy.getEncodingPattern(digit4);
        String result6 = spy.getEncodingPattern(digit5);
        String result7 = spy.getEncodingPattern(digit6);
        String result8 = spy.getEncodingPattern(digit7);
        String result9 = spy.getEncodingPattern(digit8);
        String result10 = spy.getEncodingPattern(digit9);
        // Assert
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        assertEquals(expResult5, result5);
        assertEquals(expResult6, result6);
        assertEquals(expResult7, result7);
        assertEquals(expResult8, result8);
        assertEquals(expResult9, result9);
        assertEquals(expResult10, result10);
    }

    /**
     * Test of getLCode method, of class Codes_XML_DAO.
     */
    @DisplayName("Get L Code")
    @Test
    public void testGetLCode() {
        // Arrange
        Logger.post("Testing getLCode method.");
        int digit0 = 0;
        int digit1 = 1;
        int digit2 = 2;
        int digit3 = 3;
        int digit4 = 4;
        int digit5 = 5;
        int digit6 = 6;
        int digit7 = 7;
        int digit8 = 8;
        int digit9 = 9;
        String expResult1 = "0001101";
        String expResult2 = "0011001";
        String expResult3 = "0010011";
        String expResult4 = "0111101";
        String expResult5 = "0100011";
        String expResult6 = "0110001";
        String expResult7 = "0101111";
        String expResult8 = "0111011";
        String expResult9 = "0110111";
        String expResult10 = "0001011";
        // Act
        String result1 = spy.getLCode(digit0);
        String result2 = spy.getLCode(digit1);
        String result3 = spy.getLCode(digit2);
        String result4 = spy.getLCode(digit3);
        String result5 = spy.getLCode(digit4);
        String result6 = spy.getLCode(digit5);
        String result7 = spy.getLCode(digit6);
        String result8 = spy.getLCode(digit7);
        String result9 = spy.getLCode(digit8);
        String result10 = spy.getLCode(digit9);
        // Assert
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        assertEquals(expResult5, result5);
        assertEquals(expResult6, result6);
        assertEquals(expResult7, result7);
        assertEquals(expResult8, result8);
        assertEquals(expResult9, result9);
        assertEquals(expResult10, result10);
    }

    /**
     * Test of getGCode method, of class Codes_XML_DAO.
     */
    @DisplayName("Get G Code")
    @Test
    public void testGetGCode() {
        // Arrange
        Logger.post("Testing getGCode method.");
        int digit0 = 0;
        int digit1 = 1;
        int digit2 = 2;
        int digit3 = 3;
        int digit4 = 4;
        int digit5 = 5;
        int digit6 = 6;
        int digit7 = 7;
        int digit8 = 8;
        int digit9 = 9;
        String expResult1 = "0100111";
        String expResult2 = "0110011";
        String expResult3 = "0011011";
        String expResult4 = "0100001";
        String expResult5 = "0011101";
        String expResult6 = "0111001";
        String expResult7 = "0000101";
        String expResult8 = "0010001";
        String expResult9 = "0001001";
        String expResult10 = "0010111";
        // Act
        String result1 = spy.getGCode(digit0);
        String result2 = spy.getGCode(digit1);
        String result3 = spy.getGCode(digit2);
        String result4 = spy.getGCode(digit3);
        String result5 = spy.getGCode(digit4);
        String result6 = spy.getGCode(digit5);
        String result7 = spy.getGCode(digit6);
        String result8 = spy.getGCode(digit7);
        String result9 = spy.getGCode(digit8);
        String result10 = spy.getGCode(digit9);
        // Assert
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        assertEquals(expResult5, result5);
        assertEquals(expResult6, result6);
        assertEquals(expResult7, result7);
        assertEquals(expResult8, result8);
        assertEquals(expResult9, result9);
        assertEquals(expResult10, result10);
    }

    /**
     * Test of getRCode method, of class Codes_XML_DAO.
     */
    @DisplayName("Get R Code")
    @Test
    public void testGetRCode() {
        // Arrange
        Logger.post("Testing getRCode method.");
        int digit0 = 0;
        int digit1 = 1;
        int digit2 = 2;
        int digit3 = 3;
        int digit4 = 4;
        int digit5 = 5;
        int digit6 = 6;
        int digit7 = 7;
        int digit8 = 8;
        int digit9 = 9;
        String expResult1 = "1110010";
        String expResult2 = "1100110";
        String expResult3 = "1101100";
        String expResult4 = "1000010";
        String expResult5 = "1011100";
        String expResult6 = "1001110";
        String expResult7 = "1010000";
        String expResult8 = "1000100";
        String expResult9 = "1001000";
        String expResult10 = "1110100";
        // Act
        String result1 = spy.getRCode(digit0);
        String result2 = spy.getRCode(digit1);
        String result3 = spy.getRCode(digit2);
        String result4 = spy.getRCode(digit3);
        String result5 = spy.getRCode(digit4);
        String result6 = spy.getRCode(digit5);
        String result7 = spy.getRCode(digit6);
        String result8 = spy.getRCode(digit7);
        String result9 = spy.getRCode(digit8);
        String result10 = spy.getRCode(digit9);
        // Assert
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        assertEquals(expResult5, result5);
        assertEquals(expResult6, result6);
        assertEquals(expResult7, result7);
        assertEquals(expResult8, result8);
        assertEquals(expResult9, result9);
        assertEquals(expResult10, result10);
    }
}
