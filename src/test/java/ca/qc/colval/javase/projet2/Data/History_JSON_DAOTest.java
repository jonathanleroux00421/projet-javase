/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Data;

// Project packages.
import ca.qc.colval.javase.projet2.Model.EncodingHistoryItem;
import ca.qc.colval.javase.projet2.Util.Logger;
// Junit5 packages.
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;

/**
 *
 * @author Jonathan Leroux
 */
public class History_JSON_DAOTest {
    
    static History_JSON_DAO mock = null;
    
    @BeforeAll
    public static void initAll() {
        mock = Mockito.spy(History_JSON_DAO.class);
        when(mock.findNextId()).thenReturn(2);
    }

    /**
     * Test of addEntry method, of class History_JSON_DAO.
     */
    @DisplayName("Find Next Id")
    @Test
    public void testFindNextId() {
        // Arrange
        Logger.post("Testing findNextId mock method.");
        int expResult = 2;
        // Act
        int actualResult = mock.findNextId();
        // Assert
        assertEquals(expResult, actualResult);
    }
    
    @DisplayName("Fetch One Barcode")
    @Test
    public void testFindOneBarcode() {
        // Arrange
        Logger.post("Testing fetchOneBarcode real method.");
        EncodingHistoryItem expResult = new EncodingHistoryItem(1,
                "4-12345-123456", "10101000110110011001001101111010011101011100"
                        + "111001100101011011001000010101110010011101010000101");
        // Act
        EncodingHistoryItem actualResult = mock.fetchOneBarcode(1);
        // Assert
        assertEquals(expResult.getCustomId(), actualResult.getCustomId());
        assertEquals(expResult.getBinaryBarcode(), actualResult.getBinaryBarcode());
        assertEquals(expResult.getDecimalBarcode(), actualResult.getDecimalBarcode());
    }
}
