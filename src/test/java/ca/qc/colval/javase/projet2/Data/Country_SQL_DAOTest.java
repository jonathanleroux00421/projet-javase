/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.qc.colval.javase.projet2.Data;

// Project packages.
import ca.qc.colval.javase.projet2.Model.CountryListItem;
import ca.qc.colval.javase.projet2.Util.Logger;
// Java Utils.
import edu.emory.mathcs.backport.java.util.Arrays;
import java.util.List;
// Junit5 packages.
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
// Mockito packages.
import org.mockito.Mockito;
import static org.mockito.Mockito.when;

/**
 *
 * @author Jonathan Leroux
 */
public class Country_SQL_DAOTest {
    
    static Country_SQL_DAO mock = null;

    /**
     * Test of createConnection method, of class Country_SQL_DAO.
     */
    @BeforeAll
    public static void initAll() {
        mock = Mockito.mock(Country_SQL_DAO.class);
        when(mock.fetchAllCountries())
                .thenReturn(Arrays.asList(new Integer[]{1}));
        when(mock.fetchAllCountriesAndCodes()).thenReturn(Arrays
                .asList(new CountryListItem[]{new CountryListItem("null", 0)}));
        when(mock.fetchAllCodesByCountryName("Canada"))
                .thenCallRealMethod();
        mock.createConnection();
    }

    /**
     * Test of fetchAllCountries method, of class Country_SQL_DAO.
     */
    @DisplayName("Fetch All Countries")
    @Test
    public void testFetchAllCountries() {
        // Arrange
        Logger.post("Testing fetchAllCountries mock method.");
        List<String> expResult = Arrays.asList(new Integer[]{1});
        // Act
        List<String> result = mock.fetchAllCountries();
        // Assert
        assertEquals(expResult, result);
    }

    /**
     * Test of fetchAllCodesByCountryName method, of class Country_SQL_DAO.
     */
    @DisplayName("Fetch All Codes By Country Name")
    @Test
    public void testFetchAllCodesByCountryName() {
        // Arrange
        Logger.post("Testing fetchAllCodesByCountryName real method.");
        String country = "Canada";
        mock = Mockito.spy(Country_SQL_DAO.class);
        List<Integer> expResult = Arrays.asList(new Integer[]{249, 4, 514});
        // Act
        List<Integer> result = mock.fetchAllCodesByCountryName(country);
        // Assert
        assertEquals(expResult, result);
    }

    /**
     * Test of fetchAllCountriesAndCodes method, of class Country_SQL_DAO.
     */
    @DisplayName("Fetch All Countries And Codes")
    @Test
    public void testFetchAllCountriesAndCodes() {
        // Arrange
        Logger.post("Testing fetchAllCountriesAndCodes mock method.");
        List<CountryListItem> expResult = Arrays
                .asList(new CountryListItem[]{new CountryListItem("null", 0)});
        // Act
        List<CountryListItem> result = mock.fetchAllCountriesAndCodes();
        // Assert
        assumeTrue(expResult.get(0).getName().equals(result.get(0).getName()));
        assumeTrue(expResult.get(0).getCode() == result.get(0).getCode());
    }

    /**
     * Test of closeConnection method, of class Country_SQL_DAO.
     */
    @AfterAll
    public static void closeConnection() {
        mock.closeConnection();
    }
}
